import re

file = open("tabela.txt", "r", encoding="utf8")
content = file.read()

listOfOff = re.findall("(Wyślij OFF){1}.*([0-9]{4}-[0-9]{2}-[0-9]{2})\n.*([0-9]{2}:[0-9]{2}:[0-9]{2}).*([0-9]{4}-[0-9]{2}-[0-9]{2})\n.*([0-9]{2}:[0-9]{2}:[0-9]{2}).*([0-9]{3}[|]{1}[0-9]{3}).*([0-9]{3}[|]{1}[0-9]{3})", content)

temp = [list(x) for x in listOfOff]
listOfOff = temp



with open("mojeWioskiZOffami.txt", "r", encoding="utf-8") as f:
    listaZPrzegladu = f.readlines()


listaWszystkichOffow = []
for i in range(len(listaZPrzegladu)):
    output = re.search("[0-9]{3}[|]{1}[0-9]{3}", listaZPrzegladu[i])
    if output:
        listaWszystkichOffow.append(output[0] )


wolne = []

for i in range(len(listaWszystkichOffow)):
    listaWszystkichOffow[i] = listaWszystkichOffow[i][:7]
    obecny = False
    for j in range(len(listOfOff)):
        if listaWszystkichOffow[i] == listOfOff[j][5]:
            obecny = True
    if not obecny:
        wolne.append(listaWszystkichOffow[i])

with open("listaCoordowZWolnymiOffami.txt", "w") as f1:
    for s in wolne:
        f1.write(str(s) + "\n")
print("znaleziono", len(wolne), "wolnych offów")
print("\n")
print("Program napisany przez gracza Irutar dla świata 165")
print("\n")
input("Naciśnij cokoliwek aby wyjść")
